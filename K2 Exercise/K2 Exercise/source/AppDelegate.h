//
//  AppDelegate.h
//  K2 Exercise
//
//  Created by Alex Rodrigues on 7/5/16.
//  Copyright © 2016 Alex Rodrigues. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

